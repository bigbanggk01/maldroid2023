import csv
import psycopg2
import re


pattern = r'[^a-zA-Z0-9.]'

def has_special_characters(string):
    pattern = r'[^a-zA-Z0-9.]'
    match = re.search(pattern, string)
    if match:
        return True
    return False

def starts_with_number_regex(string):
    pattern = r'^\d'
    match = re.match(pattern, string)
    return match is not None

connection = psycopg2.connect(
    host="localhost",
    database="maldroid",
    user="postgres",
    password="postgres"
)

dataset_path = "feature_vectors_static.csv"

with open(dataset_path, "r") as file:
    reader = csv.reader(file)
    header = next(reader)
header = [column.replace("Class", "class_") for column in header]
header.pop(0)

real_header = ["android.intent.action.SEND_MULTIPLE", 
               "android.permission.GET_PACKAGE_SIZE", 
               "android.permission.MODIFY_PHONE_STATE",
               "android.permission.CHANGE_COMPONENT_ENABLED_STATE",
               "android.permission.CLEAR_APP_CACHE",
               "android.permission.READ_CONTACTS",
               "android.Manifest.permission.DEVICE_POWER",
               "android.permission.DEVICE_POWER",
               "android.permission.HARDWARE_TEST",
               "android.permission.WRITE_EXTERNAL_STORAGE",
               "android.permission.ACCESS_FINE_LOCATION",
               "android.permission.BROADCAST_SMS",
               "android.permission.ACCESS_SURFACE_FLINGER",
               "android.permission.READ_FRAME_BUFFER",
               "android.permission.CHANGE_WIFI_STATE",
               "android.permission.CLEAR_APP_USER_DATA",
               "com.android.launcher.permission.CLEAR_APP_USER_DATA",
               "android.permission.CHANGE_CONFIGURATION",
               "android.permission.MOUNT_FORMAT_FILESYSTEMS",
               "android.permission.CALL_PHONE",
               "com.android.launcher.permission.CALL_PHONE",
               "com.android.voicemail.permission.ADD_VOICEMAIL",
               "android.permission.BIND_ACCESSIBILITY_SERVICE",
               "android.permission.CONTROL_LOCATION_UPDATES",
               "android.permission.BROADCAST_WAP_PUSH",
               "android.intent.action.BATTERY_OKAY",
               "android.permission.RECEIVE_MMS",
               "android.intent.action.SCREEN_ON",
               "android.permission.PERSISTENT_ACTIVITY",
               "android.permission.STATUS_BAR",
               "android.permission.BIND_DEVICE_ADMIN",
               "android.permission.WRITE_GSERVICES",
               "android.permission.BLUETOOTH_ADMIN",
               "android.permission.REBOOT",
               "android.permission.WRITE_SETTINGS",
               "android.launcher.permission.WRITE_SETTINGS",
               "android.intent.action.SENDTO",
               "android.permission.DUMP",
               "android.permission.RECEIVE_WAP_PUSH",
               "android.permission.BIND_WALLPAPER",
               "android.permission.CALL_PRIVILEGED",
               "android.permission.READ_USER_DICTIONARY",
               "android.permission.READ_SOCIAL_STREAM",
               "android.permission.BIND_INPUT_METHOD",
               "android.permission.SET_WALLPAPER",
               "android.permission.WRITE_PROFILE",
               "android.permission.REORDER_TASKS",
               "android.intent.action.NEW_OUTGOING_CALL",
               "android.intent.action.PACKAGE_CHANGED",
               "android.permission.DELETE_PACKAGES",
               "android.permission.GLOBAL_SEARCH",
               "android.permission.GET_TASKS",
               "android.permission.READ_SYNC_STATS",
               "android.permission.BROADCAST_STICKY",
               "android.permission.MODIFY_AUDIO_SETTINGS",
               "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS",
               "android.permission.NFC",
               "android.permission.WRITE_SYNC_SETTINGS",
               "android.permission.INSTALL_PACKAGES",
               "android.permission.AUTHENTICATE_ACCOUNTS",
               "android.permission.WRITE_SMS",
               "android.permission.READ_SYNC_SETTINGS",
               "android.permission.MANAGE_ACCOUNTS",
               "android.permission.USE_CREDENTIALS",
               "android.intent.action.BOOT_COMPLETED",
               "android.permission.READ_SMS",
               "android.permission.GET_ACCOUNTS",
               "android.permission.READ_PHONE_STATE"]

for i in range(0, len(real_header)):
    real_header[i] = "\"" + real_header[i] + "\""

for h in header:
    if "READ_PHONE_STATE" in h:
        print(h)
    if not has_special_characters(h) and not starts_with_number_regex(h):
        if h.startswith("."):
            h = h[1:]
        if h != "":
            real_header.append("\"" + h + "\"")

cursor = connection.cursor()
table_name = "static"
create_table_query = f"CREATE TABLE {table_name} ({', '.join(f'{column} VARCHAR' for column in real_header)});"
cursor.execute(create_table_query)
connection.commit()
cursor.close()
connection.close()