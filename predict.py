import pickle
import pandas as pd
from androguard.core.bytecodes.apk import APK
from androguard.core.bytecodes import dvm
import numpy as np

def extract_permissions(apk_file):    
    apk = APK(apk_file)
    d = dvm.DalvikOdexVMFormat(apk.get_dex())
    classes = d.get_classes()
    apis = []
    for class_obj in classes:
        for method in class_obj.get_methods():
            if method.get_access_flags() & 0x0100:
                class_name = class_obj.get_name()
                method_name = method.get_name()
                apis.append(f"{class_name}.{method_name}")

    metrics = apk.get_permissions() + apk.get_activities() + apk.get_services() + apis
    return metrics

apk_file_path = input()
metrics = extract_permissions(apk_file_path)
with open('model.pkl', 'rb') as file:
    ensemble_classifier = pickle.load(file)

static_set = None
with open('st_col.pkl', 'rb') as file:
    static_set = pickle.load(file)
    file.close()

vector = [1 if metric in metrics else 0 for metric in static_set]
new_data = pd.DataFrame([vector], columns=list(static_set))
predictions = ensemble_classifier.predict(new_data)
if predictions[0] == 1:
    print('Ransomware')
if predictions[0] == 0:
    print('Benign')
