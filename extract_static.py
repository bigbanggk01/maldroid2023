import os
import pandas as pd
import pickle
from androguard.core.bytecodes.apk import APK
from androguard.core.bytecodes import dvm

def extract_permissions(apk_file):    
    apk = APK(apk_file)
    d = dvm.DalvikOdexVMFormat(apk.get_dex())
    classes = d.get_classes()
    apis = []
    for class_obj in classes:
        for method in class_obj.get_methods():
            if method.get_access_flags() & 0x0100:
                class_name = class_obj.get_name()
                method_name = method.get_name()
                apis.append(f"{class_name}.{method_name}")

    metrics = apk.get_permissions() + apk.get_activities() + apk.get_services() + apis
    return metrics

apk_directories = ['./apk/ransomware', './apk/benign']
static_set = set()
static_vectors = []

for apk_directory in apk_directories:
    for filename in os.listdir(apk_directory):
        apk_file_path = os.path.join(apk_directory, filename)
        static_set.update(extract_permissions(apk_file_path))

for apk_directory in apk_directories:
    apk_class = ''
    if apk_directory == './apk/ransomware':
        apk_class = '1'
    else:
        apk_class = '0'
    for filename in os.listdir(apk_directory):
        apk_file_path = os.path.join(apk_directory, filename)
        metrics = extract_permissions(apk_file_path)
        vector = [1 if metric in metrics else 0 for metric in static_set]
        vector.append(apk_class)
        static_vectors.append(vector)
        
df = pd.DataFrame(static_vectors, columns=list(static_set) + ['_class'])
df.to_csv('dataset.csv', index=False)

print(df)
with open('st_col.pkl', 'wb') as file:
  pickle.dump(static_set, file)
  file.close()