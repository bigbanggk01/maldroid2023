import os
import pandas as pd
from collections import Counter
# process_frequency = {}
import pickle

def contains_all_non_numeric(string):
    for char in string:
        if char.isdigit():
            return False
    return True

log_directories = ['./log/ransomware', './log/benign']
dynamic_set = set()
dynamic_vectors = []

def parse(file):
    with open(file, "r", encoding="utf-8") as file:
        sc_items = []
        for line in file:
            sc = line.split(" ")
            if len(sc) > 7 and contains_all_non_numeric(sc[8]) and len(sc[8])>3:
                sc[8] = sc[8].replace(':', '')
                sc_items.append(sc[8])
        frequency_count = Counter(sc_items)
        
    return frequency_count

for log_directory in log_directories:
    for filename in os.listdir(log_directory):
        log_file_path = os.path.join(log_directory, filename)
        frequency_count = parse(log_file_path)
        dynamic_set.update(frequency_count)

for log_directory in log_directories:
    apk_class = '1' if log_directory == './log/ransomware' else '0'
    for filename in os.listdir(log_directory):
        log_file_path = os.path.join(log_directory, filename)
        frequency_count = parse(log_file_path)
        dynamic_vector = {column: frequency_count.get(column, 0) for column in dynamic_set}
        dynamic_vector['_class'] = apk_class
        dynamic_vectors.append(dynamic_vector)
                

df = pd.DataFrame(dynamic_vectors, columns=list(dynamic_set) + ['_class'])
df.to_csv('dy_dataset.csv', index=False)
with open('dy_col_set.pkl', 'wb') as file:
    pickle.dump(dynamic_set, file)