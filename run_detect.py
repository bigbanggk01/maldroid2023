import subprocess
import os
import time
from androguard.core.bytecodes.apk import APK
from collections import Counter
import pickle
import pandas as pd
from androguard.core.bytecodes.apk import APK
from androguard.core.bytecodes import dvm

apk_file = input("Your apk: ")
log_dir = 'tmp/'+apk_file+'.txt'
device_id = "192.168.39.108:5555"
vm_name = 'Custom Phone_1'
apk = APK(apk_file)

package_name = apk.get_package()


def extract_permissions():    
    d = dvm.DalvikOdexVMFormat(apk.get_dex())
    classes = d.get_classes()
    apis = []
    for class_obj in classes:
        for method in class_obj.get_methods():
            if method.get_access_flags() & 0x0100:
                class_name = class_obj.get_name()
                method_name = method.get_name()
                apis.append(f"{class_name}.{method_name}")

    metrics = apk.get_permissions() + apk.get_activities() + apk.get_services() + apis
    return metrics

def contains_all_non_numeric(string):
    for char in string:
        if char.isdigit():
            return False
    return True

def parse(file):
    with open(file, "r", encoding="utf-8") as file:
        sc_items = []
        for line in file:
            sc = line.split(" ")
            if len(sc) > 7 and contains_all_non_numeric(sc[8]) and len(sc[8])>3:
                sc[8] = sc[8].replace(':', '')
                sc_items.append(sc[8])
        frequency_count = Counter(sc_items)
        
    return frequency_count

def start_apk(package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "monkey", "-p", package_name
                    , "-c", 
                    "android.intent.category.LAUNCHER", "1"
                    ], check=True)


def stop_apk(package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "am", "force-stop", package_name], check=True)

def capture_system_api_calls():
    with open(log_dir, "w") as f:
        subprocess.Popen(["adb", "logcat", "-v", "json", "-d"], stdout=f, universal_newlines=True)

def install_and_capture(apk_path, package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "install", apk_path], check=True)
    start_apk(package_name, device_id)
    time.sleep(10)
    capture_system_api_calls()

metrics = extract_permissions()
with open('model.pkl', 'rb') as file:
    st_ensemble_classifier = pickle.load(file)
    file.close()

with open('st_col.pkl', 'rb') as file:
    static_set = pickle.load(file)
    file.close()

vector = [1 if metric in metrics else 0 for metric in static_set]
new_data = pd.DataFrame([vector], columns=list(static_set))
st_predictions = st_ensemble_classifier.predict(new_data)

install_and_capture(apk_file, package_name, device_id)

with open('dy_col_set.pkl', 'rb') as file:
    dynamic_set = pickle.load(file)
    file.close()

for filename in os.listdir():
    frequency_count = parse(log_dir)
    dynamic_vector = {column: frequency_count.get(column, 0) for column in dynamic_set}

new_data = pd.DataFrame([dynamic_vector], columns=list(dynamic_set))
with open('dy_model.pkl', 'rb') as file:
    dy_ensemble_classifier, top_k_indices = pickle.load(file)
    file.close()
new_data = new_data.values[:,top_k_indices]
dy_predictions = dy_ensemble_classifier.predict(new_data)

if st_predictions[0] == 0 and dy_predictions[0] == 0:
    print('[+] Result: ' + 'Benign')
else:
    print('[+] Result: ' + 'Ransomware')