import subprocess
import os
import time
from androguard.core.bytecodes.apk import APK
import psutil

apk_directory = "./apk/benign"
apk_file = "com.buzzhives.android.tripplanner.apk"
device_id = "192.168.39.108:5555"
vm_name = 'Custom Phone_1'
apk_path = os.path.join(apk_directory, apk_file)
apk = APK(apk_path)

package_name = apk.get_package()

def start_apk(package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "monkey", "-p", package_name
                    , "-c", 
                    "android.intent.category.LAUNCHER", "1"
                    ], check=True)


def stop_apk(package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "am", "force-stop", package_name], check=True)

def capture_system_api_calls():
    with open('log/ransomware/logcat_'+apk_file+'.txt', "w") as f:
        subprocess.Popen(["adb", "logcat", "-v", "json", "-d"], stdout=f, universal_newlines=True)

def grant_admin_permission(package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "pm", "grant", package_name, "android.permission.BIND_DEVICE_ADMIN"], check=True)

def capture_cpu_usage(duration_seconds):
    end_time = time.time() + duration_seconds

    while time.time() < end_time:
        cpu_percent = psutil.cpu_percent(interval=1)
        print(f'CPU Usage: {cpu_percent}%')
        time.sleep(1)

def perform_factory_reset(device_id):
    subprocess.run(["adb", "-s", device_id, "shell", "recovery", "-w"], check=True)

def install_and_capture(apk_path, package_name, device_id):
    subprocess.run(["adb", "-s", device_id, "install", apk_path], check=True)
    start_apk(package_name, device_id)
    time.sleep(10)
    capture_system_api_calls()

install_and_capture(apk_path, package_name, device_id)